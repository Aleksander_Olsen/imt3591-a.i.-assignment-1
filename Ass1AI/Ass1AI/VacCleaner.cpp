#include "VacCleaner.h"



VacCleaner::VacCleaner() {
	posX = 0;
	posY = 0;
	clip.x = 0;
	clip.y = 0;
	clip.w = clipSize;
	clip.h = clipSize;
}

VacCleaner::VacCleaner(std::shared_ptr<Cell> _cell) {
	posX = _cell->getX();
	posY = _cell->getY();
	clip.x = 0;
	clip.y = 0;
	clip.w = clipSize;
	clip.h = clipSize;
	currentCell = _cell;
	currentCell->setVisited();
}


VacCleaner::~VacCleaner()
{
}

//The robot do the thinking. 
//If the cell is dirty, clean it. If not, move to next cell.
int VacCleaner::think(int step) {
	
	if (currentCell->isDirty()) {
		clean();
		std::cout << "Step " << step << ": Vacuum cleaner cleand the cell.\n";
	} else {
		step = move(step);
	}

	return step;
}

//Set the current cell to clean.
void VacCleaner::clean() {
	currentCell->clean();
}

//The movement have this priority list (going from highest to lowest):
//RIGHT -> DOWN -> UP -> LEFT -> UP(if no other choice) -> LEFT (if no other choice)
//This will result in the vacuum always ending at position (0,0), even if it has already been visited.
int VacCleaner::move(int step) {
	//Temp holder for the cell it is at now.
	std::shared_ptr<Cell> tempCell = currentCell;

	int dir = RIGHT;
	
	while ((currentCell == nullptr || currentCell->isVisited()) && dir < LAST) {
		currentCell = tempCell->getNeighbors().at(dir);
		if (currentCell == nullptr) {
			std::cout << "Step " << step << ": Tried to move " << dir << ", but no cell was there. Moving back\n";
			++step;
		} else if (currentCell->isVisited()) {
			std::cout << "Cell already visited\n";
		} else {
			std::cout << "Step " << step << ": moved " << dir << "\n";
			currentCell->setVisited();
			posX = currentCell->getX();
			posY = currentCell->getY();
			return step;
		}

		++dir;

		//When all cells around the current one is empty or already visited, start moving UP and LEFT.
		if (dir >= LAST) {
			if (tempCell->getNeighbors().at(UP) != nullptr) {
				currentCell = tempCell->getNeighbors().at(UP);
				dir = UP;
			} else if (tempCell->getNeighbors().at(LEFT) != nullptr) {
				currentCell = tempCell->getNeighbors().at(LEFT);
				dir = LEFT;
			} else {
				std::cout << "Finished, all cells visited and cleaned...\n\n";
				step = MAX_STEPS - 1;
				return step;
			}

			std::cout << "Step " << step << ": moved " << dir << "\n";
			currentCell->setVisited();
			posX = currentCell->getX();
			posY = currentCell->getY();
			return step;
		}
	}

	return step;
}


//Load the vacuum cleaner image.
bool VacCleaner::loadMedia(std::unique_ptr<SDLClass> &SDL_Class, SDL_Renderer* gRenderer) {
	
	SDL_Class->FreeTexture(sprite);
	sprite = SDL_Class->LoadTexture(vacuumFile, gRenderer);

	return sprite != NULL;
}

//Render the vacuum to screen.
void VacCleaner::render(std::unique_ptr<SDLClass> &SDL_Class, SDL_Renderer * gRenderer) {
	SDL_Class->Render(CELL_CLIP_SIZE * posX + 15, CELL_CLIP_SIZE * posY + 15, sprite, clip, gRenderer);
}



std::string VacCleaner::getFilepath()
{
	return vacuumFile;
}

