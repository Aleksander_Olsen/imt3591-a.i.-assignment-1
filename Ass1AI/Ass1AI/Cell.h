#pragma once

#include <iostream>
#include <vector>
#include <memory>

#include "Globals.h"

class Cell {
private:
	bool dirty;
	bool visited;
	int posX;
	int posY;
	//UP, DOWN, LEFT, RIGHT
	std::vector<std::shared_ptr<Cell>> neighbors;
public:
	Cell();
	Cell(int x, int y);
	~Cell();
	void clean();
	void makeDirty();
	void initNeighbors(std::vector<std::vector<std::shared_ptr<Cell>> > &_map);
	void writeNeighbors();
	void setVisited();
	void writeCell();
	int getX();
	int getY();
	bool isDirty();
	bool isVisited();
	std::vector<std::shared_ptr<Cell>> getNeighbors();

};

