#include "Cell.h"



Cell::Cell() {
	posX = 0;
	posY = 0;
	dirty = false;
	visited = false;
	for (int i = 0; i < 4; i++) {
		neighbors.push_back(nullptr);
	}
}

Cell::Cell(int x, int y) {

	posX = x;
	posY = y;
	dirty = false;
	visited = false;
	for (int i = 0; i < 4; i++) {
		neighbors.push_back(nullptr);
	}

}


Cell::~Cell()
{
}

void Cell::clean() {
	dirty = false;
}

void Cell::makeDirty() {
	dirty = true;
}

//Initiate pointers to the cells at RIGHT, LEFT, UP, DOWN for this cell.
//If there is no cell, the pointer stay as nullptr.
void Cell::initNeighbors(std::vector<std::vector<std::shared_ptr<Cell>> > &_map) {
	int x = posX;
	int y = posY;
	
	if ((x + 1) < MAP_SIZE_X) {
		neighbors.at(RIGHT) = _map.at(x + 1).at(y);
	}

	if ((x - 1) >= 0) {
		neighbors.at(LEFT) = _map.at(x - 1).at(y);
	}
	
	if ((y + 1) < MAP_SIZE_Y) {
		neighbors.at(DOWN) = _map.at(x).at(y + 1);
	}
	
	if ((y - 1) >= 0) {
		neighbors.at(UP) = _map.at(x).at(y - 1);
	}
}

void Cell::writeNeighbors() {
	for (auto &x : neighbors) {
		if (x != nullptr) {
			std::cout << " (" << x->getX() << "," << x->getY() << ") ";
		} else {
			std::cout << " NULL ";
		}
	}
}

void Cell::setVisited() {
	visited = true;
}

void Cell::writeCell() {
	std::cout << "Cell (" <<posX << "," << posY << ")  ";
	isVisited() == true ? std::cout << "VISITED " : std::cout << "NOT VISITED ";
	isDirty() == true ? std::cout << "dirty" : std::cout << "clean";
	std::cout << "  Neighbors: ";
	writeNeighbors();
	std::cout << "\n";
}

int Cell::getX()
{
	return posX;
}

int Cell::getY()
{
	return posY;
}

bool Cell::isDirty()
{
	return dirty;
}

bool Cell::isVisited()
{
	return visited;
}

std::vector<std::shared_ptr<Cell>> Cell::getNeighbors()
{
	return neighbors;
}
