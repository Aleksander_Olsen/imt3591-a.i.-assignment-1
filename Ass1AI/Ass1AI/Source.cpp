//Main source file.


#include "Globals.h"
#include "VacCleaner.h"
#include "Cell.h"
#include "SDLClass.h"


int step = 0;
bool inMotion;
std::vector<std::vector<std::shared_ptr<Cell>> > map;
std::unique_ptr<VacCleaner> cleaner;
std::unique_ptr<SDLClass> SDL_Class;

SDL_Texture* cellSprite;
SDL_Texture* cellSpriteVisited;
SDL_Texture* dirt;
SDL_Texture* spriteRender;
SDL_Rect cellClip;

//Bool controlling when to stop the program.
auto running = true;

//SDL event handler.
SDL_Event event;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The SDL renderer.
SDL_Renderer* gRenderer = NULL;

//Starts up SDL and creates window
bool init();

void initMap();

bool loadMedia();

//Frees media and shuts down SDL
void close();

bool init() {
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	} else {
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Create window
		gWindow = SDL_CreateWindow("Assignment 1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else {

			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL) {
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else {
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags)) {
					printf("SDL_image could not initialize! SDL_mage Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

void initMap() {
	int randX;
	int randY;

	//Initialize map
	for (int i = 0; i < MAP_SIZE_X; i++) {
		std::vector<std::shared_ptr<Cell>> column;
		for (int j = 0; j < MAP_SIZE_Y; j++) {
			column.push_back(std::make_shared<Cell>(i, j));
		}
		map.push_back(std::move(column));
	}

	for (auto &x : map) {
		for (auto &y : x) {
			y->initNeighbors(map);
		}
	}


	//Initialize dirt.
	for (int i = 0; i < DIRTY_CELLS; i++) {
		randX = rand() % MAP_SIZE_X;
		randY = rand() % MAP_SIZE_Y;
		while (map.at(randX).at(randY)->isDirty()) {
			randX = rand() % MAP_SIZE_X;
			randY = rand() % MAP_SIZE_Y;
		}
		map.at(randX).at(randY)->makeDirty();
	}
}


bool loadMedia() {
	//Loading success flag
	bool success = true;

	//Load sprite sheet texture
	if (!cleaner->loadMedia(SDL_Class, gRenderer)) {
		printf("Failed to load sprite sheet texture!\n");
		success = false;
	}

	SDL_Class->FreeTexture(cellSprite);
	cellSprite = SDL_Class->LoadTexture(cellFilepath, gRenderer);

	if (cellSprite == NULL) {
		std::cout << "Failed to load cell image";
		success = false;
	}

	SDL_Class->FreeTexture(cellSpriteVisited);
	cellSpriteVisited = SDL_Class->LoadTexture(cellVisitedFilepath, gRenderer);

	if (cellSpriteVisited == NULL) {
		std::cout << "Failed to load cell image";
		success = false;
	}

	SDL_Class->FreeTexture(dirt);
	dirt = SDL_Class->LoadTexture(dirtFilepath, gRenderer);

	if (dirt == NULL) {
		std::cout << "Failed to load cell image";
		success = false;
	}

	cellClip.x = 0;
	cellClip.y = 0;
	cellClip.w = CELL_CLIP_SIZE;
	cellClip.h = CELL_CLIP_SIZE;

	SDL_Texture* spriteRender = NULL;

	return success;
}



void close() {

	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Destroy renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

int main(int argc, char* args[]) {

	//Start up SDL and create window
	if (!init()) {
		printf("Failed to initialize!\n");
	} else {

		//Initiate map and vacuum cleaner.
		initMap();
		//Set the vacuum start position (X,Y)
		cleaner = std::make_unique<VacCleaner>(map.at(VACUUM_START_X).at(VACUUM_START_Y));

		SDL_Class = std::make_unique<SDLClass>();

		inMotion = true;

		if (!loadMedia()) {
			printf("Failed to load media!\n");
		}

		
		//Print out each cell with dirty/clean status.
		for (auto &x : map) {
			for (auto &y : x) {
				y->writeCell();
			}
		}

		std::cout << "\n";
		

		while (running) {

			//Clear screen
			SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0x00);
			SDL_RenderClear(gRenderer);

			
			//Check for input.
			while (SDL_PollEvent(&event)) {
				if (event.type == SDL_QUIT) {
					running = false;
				}
				if (event.type == SDL_KEYDOWN) {
					switch (event.key.keysym.sym) {
					case SDLK_ESCAPE:
						running = false;
						break;
					default:
						inMotion = false;
					}
				}
			}

			//Do operation for this step, if not exceeded maximum number of steps.
			if (step < MAX_STEPS && !inMotion) {
				step = cleaner->think(step);
				++step;
				inMotion = true;
			}

			//Redner each cell.
			for (auto &x : map) {
				for (auto &y : x) {
					if (y->isVisited()) {
						 spriteRender = cellSpriteVisited;
					} else {
						spriteRender = cellSprite;
					}

					SDL_Class->Render(y->getX()*CELL_CLIP_SIZE, y->getY()*CELL_CLIP_SIZE, spriteRender, cellClip, gRenderer);

					if (y->isDirty()) {
						SDL_Class->Render(y->getX()*CELL_CLIP_SIZE, y->getY()*CELL_CLIP_SIZE, dirt, cellClip, gRenderer);
					}
				}
			}
			
			//Render vacuum.
			cleaner->render(SDL_Class, gRenderer);

			//Update screen
			SDL_RenderPresent(gRenderer);

		}
	}

	//Free resources and close SDL
	close();

	return 0;
}