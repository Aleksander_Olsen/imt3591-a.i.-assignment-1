#pragma once

#include "Globals.h"
#include <iostream>
#include <vector>
#include <memory>
#include "SDLClass.h"


class VacCleaner {
private:
	const int clipSize = 75;
	const std::string vacuumFile = "images/vacuum.png";

	int posX;
	int posY;
	std::shared_ptr<Cell> currentCell;
	SDL_Texture* sprite;
	SDL_Rect clip;
public:
	VacCleaner();
	VacCleaner(std::shared_ptr<Cell> _cell);
	~VacCleaner();
	int think(int step);
	void clean();
	int move(int step);
	bool loadMedia(std::unique_ptr<SDLClass> &SDL_Class, SDL_Renderer* gRenderer);
	void render(std::unique_ptr<SDLClass> &SDL_Class, SDL_Renderer* gRenderer);
	std::string getFilepath();
};

