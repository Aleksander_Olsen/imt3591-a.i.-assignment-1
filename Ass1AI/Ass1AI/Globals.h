#pragma once


#include "Cell.h"
#include <SDL.h>
#include <SDL_image.h>

enum DIRECTION { RIGHT, DOWN, UP, LEFT, LAST };

//Screen dimension constants
const auto SCREEN_WIDTH = 1000;
const auto SCREEN_HEIGHT = 1000;

const auto MAP_SIZE_X = 4;
const auto MAP_SIZE_Y = 4;
const auto DIRTY_CELLS = rand() % (MAP_SIZE_X * MAP_SIZE_Y) + 1;
const auto MAX_STEPS = 1000;
const auto CELL_CLIP_SIZE = 100;
const auto VACUUM_START_X = 0; // rand() % (MAP_SIZE_X);
const auto VACUUM_START_Y = 0; // rand() % (MAP_SIZE_Y);

const std::string cellFilepath		  = "images/Cell.png";
const std::string cellVisitedFilepath = "images/CellVis.png";
const std::string dirtFilepath = "images/Dirt.png";